<?php 
	
    /*
        PAGE NAME: bovitz.php
        PURPOSE: Gives an overview of the specific word product itself.
    */
    
	$title ="Citizen Best &ge; Brand Design Agency";
	require_once('assets/includes/header.php'); 
	
?>

			<!-- Bovitz-->
			<div class="max_width center-block bovitz">
				
				<div class="row client_header_hero">
					<div class="col-xs-12 col-sm-12 col-md-6 client_header_left">
						<h1 class="client_header">Bovitz</h1>
					</div>
					<div class="hidden-xs hidden-sm col-md-6 client_header_right">
						<a href="philzcoffee.php"><span class="dot_nav"></span></a>
						<a href="littlelotus.php"><span class="dot_nav"></span></a>
						<a href="forthright.php"><span class="dot_nav"></span></a>
						<a href="tinyprints.php"><span class="dot_nav"></span></a>
						<a href="slideranch.php"><span class="dot_nav"></span></a>
						<span class="dot_nav_current"></span><!-- Bovitz -->
						<a href="weddingpaper.php"><span class="dot_nav"></span></a>
						<a href="norsuhaus.php"><span class="dot_nav"></span></a>
						<a href="popsugar.php"><span class="dot_nav"></span></a>
						<a href="americanforests.php"><span class="dot_nav"></span></a>				
					</div>
				</div>
		
				<div class="row">
					<div class="col-xs-12 bv_hero"></div>
				</div>
				
				<div class="row intro_text_bbox">
					<div class="col-xs-12 col-sm-6 intro_text_left">
						<p>A market research company that drives profits by putting people first.</p>
					</div>
					<div class="col-xs-12 col-sm-6 intro_text_right">
						<p>Market research company Bovitz brings into alignment what customers want in their goods and services with what drives profits for a corporation. Despite their standout solutions, their branding failed to communicate their differentiating factors. Our research revealed that the heart of their success was delivering remarkable insights from customers who changed the course of a company’s trajectory. Pair that with an amazing “People First” philosophy that permeates the company’s culture and we could see a client list that was the envy of the industry.</p>
						<p>After working with Citizen Best to refine their positioning statement and brand messaging, Bovitz’s overview and website is now a point of pride as they confidently continue to win new business and serve the common interests of the people and their clients.</p>		
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 bv_section1"></div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 bv_section2"></div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 bv_section3"></div>
				</div>
				
				<div class="row">
					<div class="col-xs-12 bv_section4"></div>
				</div>
				
				<div class="row arrows_bbox">
					<div class="col-xs-2 arrow_left">
						<a href="slideranch.php"><img src="assets/images/arrow_left.png" width="28" /></a>
					</div>
					<div class="col-xs-8 arrow_text">
						<p>Brand Strategy, Brand Identity, Creative Direction and Website Design</p>
						<!--<p><a class="live_website" href="http://www.bovitzinc.com">View Live Website</a></p>-->
					</div>
					<div class="col-xs-2 arrow_right">
						<a href="weddingpaper.php"><img src="assets/images/arrow_right.png" width="28" /></a>
					</div>
				</div>
				
			</div>
			
			<script>				  
				$(document).ready(function() {  

					// Enable swiping...
					$(".bovitz").swipe( {
						// Generic swipe handler for all directions
						swipeLeft:function(event, direction, distance, duration, fingerCount) {
							window.location.href = "slideranch.php";
						},
						swipeRight: function() {
							window.location.href = "weddingpaper.php";
						},
						// Default is 75px, set to 0 for demo so any distance triggers swipe
						threshold:0
					});
					
				});
			</script>

<?php 
	
	require_once('assets/includes/footer.php'); 
	
?>