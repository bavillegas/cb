<?php

    /*
        PAGE NAME: index.php
        PURPOSE: Gives a general overview about what this company does.
    */
	
	$title ="Citizen Best &ge; Brand Design Agency";
	require_once('assets/includes/header_alt.php'); 
	
?>

		<div class="center-block max_width">
					
			<div id="myCarousel" class="carousel slide vertical" data-ride="carousel" data-interval="6000">
				<!-- Carousel indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
					<li data-target="#myCarousel" data-slide-to="4"></li>
				</ol>   
				<!-- Wrapper for carousel items -->
				<div class="carousel-inner">
					<div class="item active">
						<a href="#home_intro"><img src="assets/images/home_slide1.png" alt=""></a>
					</div>
					<div class="item">
						<a href="forthright.php"><img src="assets/images/home_slide2.png" alt=""></a>
					</div>
					<div class="item">
						<a href="weddingpaper.php"><img src="assets/images/home_slide3.png" alt=""></a>
					</div>
					<div class="item">
						<a href="slideranch.php"><img src="assets/images/home_slide4.png" alt=""></a>
					</div>
					<div class="item">
				    	<a href="littlelotus.php"><img src="assets/images/home_slide5.png" alt=""></a>
					</div>
				</div>
			</div>
			
			<script>				  
				$(document).ready(function() {  

					// Enable swiping...
					$(".carousel-inner").swipe( {
						// Generic swipe handler for all directions
						swipeLeft:function(event, direction, distance, duration, fingerCount) {
							$(this).parent().carousel('next'); 
						},
						swipeRight: function() {
							$(this).parent().carousel('prev'); 
						},
						// Default is 75px, set to 0 for demo so any distance triggers swipe
						threshold:0
					});
					
				});
			</script>
			
			<div id="home_intro" class="row intro_bbox">
				<div class="col-xs-12 intro">
					<div class="home_down">
						<a class="not_button" href="#home_animation"><img class="arrow_down" src="assets/images/arrow_down.png" alt="" /></a>
					</div>
					<h1>connected thinking. connecting people.</h1>
					<p>The right strategy, creative direction and user experience results in deeper engagement and lasting connection. As a brand strategy and design agency it is our mission to help you define, redirect and retain, what we like to call, <b>brand citizenship.</b></p>
					<a href="about.php">See What We Mean</a>
				</div>
			</div>
			
			<div id="home_animation" class="row transition_bbox">
				<div class="col-xs-12 transition">
					
					<div class="transition_outer">
					
						<div class="transition_bg"></div>
						<div class="transition_overlay">
							<div class="col-xs-6 transition_header">
								<h1>We create brand</h1>
							</div>
							<div class="col-xs-6 slidingVertical">
								<span>awareness</span>
								<span>engagement</span>
								<span>trust</span>
								<span>experiences</span>
								<span>connection</span>
							</div>
						</div>
						
					</div>
					
					<div class="row transition_btn_bbox hidden-xs">
					<div class="col-xs-12 transition_btn">
						<a href="work.php">See It to Believe It</a>
					</div>
					</div>
					
				</div>
			</div>
			
			<div class="row transition_btn_mobile_bbox visible-xs">
				<div class="col-xs-12 transition_btn_mobile">
					<a href="work.php">See It to Believe It</a>
				</div>	
			</div>
			
			<script>
				$(document).ready(function() {
					
					var bgArray = ['home_transition1.png', 'home_transition2.png', 'home_transition3.png'];
					var bg = bgArray[Math.floor(Math.random() * bgArray.length)];
					var path = 'assets/images/';
					var imageUrl = path + bg;
					$('.transition_bg').css('background-image', 'url(' + imageUrl + ')');
					
				});
			</script>
			
			<div class="row team_bbox">
				<div class="col-xs-12 team">
					<h1>meet our <a href="about.php#about_team">team</a></h1>
				</div>
			</div>
				
			<div class="row hello_bbox">
				<div class="col-xs-12 hello">
					<p>together we can create citizens for your brand.<span class="visible-xs visible-sm"><br /></span>&nbsp;<a href="contact.php">Let's Start with Hello</a></p>
				</div>
			</div>
					
		</div>

<?php 
	
	require_once('assets/includes/footer.php'); 
	
?>