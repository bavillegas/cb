<?php

    /*
        PAGE NAME: work.php
        PURPOSE: Gives an overview of work product.
    */
	
	$title ="Citizen Best &ge; Brand Design Agency";
	require_once('assets/includes/header.php'); 
	
?>

		<div class="max_width center-block">
			
			<div class="row">
				<div class="col-xs-12 work_hero">
					<h1>when our clients succeed we succeed, then  it’s time for champagne</h1>
					<p>Our integration and collaboration with clients creates best practices and elevates ideas to innovation.</p>
				</div>
			</div>
			
			<div class="row work_collection visible-xs visible-sm">
				
				<div class="col-xs-12 col-sm-6 work_col">
					<div class="fr_container">
						<a href="forthright.php">
							<img class="work_imgs fr_img" src="assets/images/fr_work.png" alt="" />
						</a>
					</div>
					<div class="ll_container">
						<a href="littlelotus.php">
							<img class="work_imgs ll_img" src="assets/images/ll_work.png" alt="" />
						</a>
					</div>
					<div class="pc_container">
						<a href="philzcoffee.php">
							<img class="work_imgs pc_img" src="assets/images/pc_work.png" alt="" />
						</a>
					</div>
					<div class="tp_container">
						<a href="tinyprints.php">
							<img class="work_imgs tp_img" src="assets/images/tp_work.png" alt="" />
						</a>
					</div>
					<div class="ps_container">
						<a href="popsugar.php">
							<img class="work_imgs ps_img" src="assets/images/ps_work.png" alt="" />
						</a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 work_col">
					<div class="nh_container">
						<a href="norsuhaus.php">
							<img class="work_imgs nh_img" src="assets/images/nh_work.png" alt="" />
						</a>
					</div>
					<div class="sr_container">
						<a href="slideranch.php">
							<img class="work_imgs sr_img" src="assets/images/sr_work.png" alt="" />
						</a>
					</div>
					<div class="wp_container">
						<a href="weddingpaper.php">
							<img class="work_imgs wp_img" src="assets/images/wp_work.png" alt="" />
						</a>
					</div>
					<div class="bv_container">
						<a href="bovitz.php">
							<img class="work_imgs bv_img" src="assets/images/bv_work.png" alt="" />
						</a>
					</div>
					<div class="af_container">
						<a href="americanforests.php">
							<img class="work_imgs af_img" src="assets/images/af_work.png" alt="" />
						</a>
					</div>
				</div>
				
			</div>
			
			<div class="row work_collection hidden-xs hidden-sm">
				
				<div class="col-xs-12 col-sm-6 work_col">
					<div class="fr_container">
						<img class="work_imgs fr_img" src="assets/images/fr_work.png" alt="" />
						<div class="work_hovers fr_hover">
							<h1>Forthright</h1>
							<a class="button" href="forthright.php">View Project</a>
						</div>
					</div>
					<div class="ll_container">
						<img class="work_imgs ll_img" src="assets/images/ll_work.png" alt="" />
						<div class="work_hovers ll_hover">
							<h1>Little Lotus</h1>
							<a class="button" href="littlelotus.php">View Project</a>
						</div>
					</div>
					<div class="pc_container">
						<img class="work_imgs pc_img" src="assets/images/pc_work.png" alt="" />
						<div class="work_hovers pc_hover">
							<h1>Philz Coffee</h1>
							<a class="button" href="philzcoffee.php">View Project</a>
						</div>
					</div>
					<div class="tp_container">
						<img class="work_imgs tp_img" src="assets/images/tp_work.png" alt="" />
						<div class="work_hovers tp_hover">
							<h1>Tiny Prints</h1>
							<a class="button" href="tinyprints.php">View Project</a>
						</div>
					</div>
					<div class="ps_container">
						<img class="work_imgs ps_img" src="assets/images/ps_work.png" alt="" />
						<div class="work_hovers ps_hover">
							<h1>Popsugar Must Have</h1>
							<a class="button" href="popsugar.php">View Project</a>
						</div>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 work_col">
					<div class="nh_container">
						<img class="work_imgs nh_img" src="assets/images/nh_work.png" alt="" />
						<div class="work_hovers nh_hover">
							<h1>Norsu Haus</h1>
							<a class="button" href="norsuhaus.php">View Project</a>
						</div>
					</div>
					<div class="sr_container">
						<img class="work_imgs sr_img" src="assets/images/sr_work.png" alt="" />
						<div class="work_hovers sr_hover">
							<h1>Slide Ranch</h1>
							<a class="button" href="slideranch.php">View Project</a>
						</div>
					</div>
					<div class="wp_container">
						<img class="work_imgs wp_img" src="assets/images/wp_work.png" alt="" />
						<div class="work_hovers wp_hover">
							<h1>Wedding Paper Divas</h1>
							<a class="button" href="weddingpaper.php">View Project</a>
						</div>
					</div>
					<div class="bv_container">
						<img class="work_imgs bv_img" src="assets/images/bv_work.png" alt="" />
						<div class="work_hovers bv_hover">
							<h1>Bovitz</h1>
							<a class="button" href="bovitz.php">View Project</a>
						</div>
					</div>
					<div class="af_container">
						<img class="work_imgs af_img" src="assets/images/af_work.png" alt="" />
						<div class="work_hovers af_hover">
							<h1>American Forests</h1>
							<a class="button" href="americanforests.php">View Project</a>
						</div>
					</div>
				</div>
			</div>
						
		</div>

<?php 
	
	require_once('assets/includes/footer.php'); 
	
?>